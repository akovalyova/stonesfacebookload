<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221213123809 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE stone_photo ADD page INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stone_post ADD image TEXT NOT NULL');
        $this->addSql('ALTER TABLE "user" ADD profile_picture TEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE stone_post DROP image');
        $this->addSql('ALTER TABLE stone_photo DROP page');
        $this->addSql('ALTER TABLE "user" DROP profile_picture');
    }
}
