<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230329162638 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE stone_post_id_seq CASCADE');
        $this->addSql('ALTER TABLE stone_post DROP CONSTRAINT fk_184d34f91582d292');
        $this->addSql('ALTER TABLE stone_post DROP CONSTRAINT fk_184d34f9f675f31b');
        $this->addSql('DROP TABLE stone_post');
        $this->addSql('ALTER TABLE stone ADD fb_url TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE stone ADD fb_likes INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stone ADD fb_comments INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stone ADD fb_content TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE stone ADD fb_images_string TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE stone ADD fb_comments_details TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE stone ADD fb_image TEXT NOT NULL');
        $this->addSql('ALTER TABLE stone ADD fb_page BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE stone ADD fb_post BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE stone ADD published_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE stone_post_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE stone_post (id INT NOT NULL, stone_id INT DEFAULT NULL, author_id INT DEFAULT NULL, post BIGINT DEFAULT NULL, url TEXT DEFAULT NULL, page BIGINT DEFAULT NULL, likes INT DEFAULT NULL, comments INT DEFAULT NULL, content TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, photos TEXT DEFAULT NULL, comments_details TEXT DEFAULT NULL, image TEXT NOT NULL, published_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_184d34f9f675f31b ON stone_post (author_id)');
        $this->addSql('CREATE UNIQUE INDEX uniq_184d34f91582d292 ON stone_post (stone_id)');
        $this->addSql('ALTER TABLE stone_post ADD CONSTRAINT fk_184d34f91582d292 FOREIGN KEY (stone_id) REFERENCES stone (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE stone_post ADD CONSTRAINT fk_184d34f9f675f31b FOREIGN KEY (author_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE stone DROP fb_url');
        $this->addSql('ALTER TABLE stone DROP fb_likes');
        $this->addSql('ALTER TABLE stone DROP fb_comments');
        $this->addSql('ALTER TABLE stone DROP fb_content');
        $this->addSql('ALTER TABLE stone DROP fb_images_string');
        $this->addSql('ALTER TABLE stone DROP fb_comments_details');
        $this->addSql('ALTER TABLE stone DROP fb_image');
        $this->addSql('ALTER TABLE stone DROP fb_page');
        $this->addSql('ALTER TABLE stone DROP fb_post');
        $this->addSql('ALTER TABLE stone DROP published_at');
    }
}
