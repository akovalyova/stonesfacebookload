<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20221212133909 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE post_code (code VARCHAR(5) NOT NULL, post_name VARCHAR(255) NOT NULL, region_name VARCHAR(255) DEFAULT NULL, town_name VARCHAR(255) DEFAULT NULL, PRIMARY KEY(code))');
        $this->addSql('CREATE TABLE stone (id INT NOT NULL, author_id INT DEFAULT NULL, parent_id INT DEFAULT NULL, post_code VARCHAR(5) DEFAULT NULL, name VARCHAR(60) DEFAULT NULL, details VARCHAR(255) DEFAULT NULL, original BOOLEAN DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1920052AF675F31B ON stone (author_id)');
        $this->addSql('CREATE INDEX IDX_1920052A727ACA70 ON stone (parent_id)');
        $this->addSql('CREATE TABLE stone_stone (stone_source INT NOT NULL, stone_target INT NOT NULL, PRIMARY KEY(stone_source, stone_target))');
        $this->addSql('CREATE INDEX IDX_4E0A775771695880 ON stone_stone (stone_source)');
        $this->addSql('CREATE INDEX IDX_4E0A7757688C080F ON stone_stone (stone_target)');
        $this->addSql('CREATE TABLE stone_comment (id INT NOT NULL, author_id INT DEFAULT NULL, content TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B0EE12EDF675F31B ON stone_comment (author_id)');
        $this->addSql('CREATE TABLE stone_location (id INT NOT NULL, stone_id INT NOT NULL, location_name VARCHAR(100) DEFAULT NULL, long NUMERIC(10, 2) DEFAULT NULL, lat NUMERIC(10, 2) DEFAULT NULL, details VARCHAR(255) DEFAULT NULL, date_located DATE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C405A03D1582D292 ON stone_location (stone_id)');
        $this->addSql('CREATE TABLE stone_photo (id INT NOT NULL, stone_id INT NOT NULL, name VARCHAR(100) DEFAULT NULL, display_name VARCHAR(100) DEFAULT NULL, extension VARCHAR(10) DEFAULT NULL, path VARCHAR(255) NOT NULL, size INT DEFAULT NULL, mime_type VARCHAR(127) DEFAULT NULL, main BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_439DF6651582D292 ON stone_photo (stone_id)');
        $this->addSql('CREATE TABLE stone_post (id INT NOT NULL, stone_id INT DEFAULT NULL, author_id INT DEFAULT NULL, post INT NOT NULL, url TEXT DEFAULT NULL, page INT NOT NULL, likes INT DEFAULT NULL, comments INT DEFAULT NULL, content TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, photos TEXT DEFAULT NULL, comments_details TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_184D34F91582D292 ON stone_post (stone_id)');
        $this->addSql('CREATE INDEX IDX_184D34F9F675F31B ON stone_post (author_id)');
        $this->addSql('COMMENT ON COLUMN stone_post.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE stone_tag (id INT NOT NULL, name VARCHAR(100) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE stone_tag_stone (stone_tag_id INT NOT NULL, stone_id INT NOT NULL, PRIMARY KEY(stone_tag_id, stone_id))');
        $this->addSql('CREATE INDEX IDX_6F88D02C943144E2 ON stone_tag_stone (stone_tag_id)');
        $this->addSql('CREATE INDEX IDX_6F88D02C1582D292 ON stone_tag_stone (stone_id)');
        $this->addSql('CREATE TABLE stone_vote (id INT NOT NULL, stone_id INT NOT NULL, author_id INT NOT NULL, comment_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_18D7DD101582D292 ON stone_vote (stone_id)');
        $this->addSql('CREATE INDEX IDX_18D7DD10F675F31B ON stone_vote (author_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_18D7DD10F8697D13 ON stone_vote (comment_id)');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, fname VARCHAR(50) DEFAULT NULL, lname VARCHAR(50) DEFAULT NULL, mname VARCHAR(50) DEFAULT NULL, page INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('ALTER TABLE stone ADD CONSTRAINT FK_1920052AF675F31B FOREIGN KEY (author_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE stone ADD CONSTRAINT FK_1920052A727ACA70 FOREIGN KEY (parent_id) REFERENCES stone (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE stone_stone ADD CONSTRAINT FK_4E0A775771695880 FOREIGN KEY (stone_source) REFERENCES stone (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE stone_stone ADD CONSTRAINT FK_4E0A7757688C080F FOREIGN KEY (stone_target) REFERENCES stone (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE stone_comment ADD CONSTRAINT FK_B0EE12EDF675F31B FOREIGN KEY (author_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE stone_location ADD CONSTRAINT FK_C405A03D1582D292 FOREIGN KEY (stone_id) REFERENCES stone (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE stone_photo ADD CONSTRAINT FK_439DF6651582D292 FOREIGN KEY (stone_id) REFERENCES stone (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE stone_post ADD CONSTRAINT FK_184D34F91582D292 FOREIGN KEY (stone_id) REFERENCES stone (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE stone_post ADD CONSTRAINT FK_184D34F9F675F31B FOREIGN KEY (author_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE stone_tag_stone ADD CONSTRAINT FK_6F88D02C943144E2 FOREIGN KEY (stone_tag_id) REFERENCES stone_tag (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE stone_tag_stone ADD CONSTRAINT FK_6F88D02C1582D292 FOREIGN KEY (stone_id) REFERENCES stone (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE stone_vote ADD CONSTRAINT FK_18D7DD101582D292 FOREIGN KEY (stone_id) REFERENCES stone (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE stone_vote ADD CONSTRAINT FK_18D7DD10F675F31B FOREIGN KEY (author_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE stone_vote ADD CONSTRAINT FK_18D7DD10F8697D13 FOREIGN KEY (comment_id) REFERENCES stone_comment (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE stone DROP CONSTRAINT FK_1920052AF675F31B');
        $this->addSql('ALTER TABLE stone DROP CONSTRAINT FK_1920052A727ACA70');
        $this->addSql('ALTER TABLE stone_stone DROP CONSTRAINT FK_4E0A775771695880');
        $this->addSql('ALTER TABLE stone_stone DROP CONSTRAINT FK_4E0A7757688C080F');
        $this->addSql('ALTER TABLE stone_comment DROP CONSTRAINT FK_B0EE12EDF675F31B');
        $this->addSql('ALTER TABLE stone_location DROP CONSTRAINT FK_C405A03D1582D292');
        $this->addSql('ALTER TABLE stone_photo DROP CONSTRAINT FK_439DF6651582D292');
        $this->addSql('ALTER TABLE stone_post DROP CONSTRAINT FK_184D34F91582D292');
        $this->addSql('ALTER TABLE stone_post DROP CONSTRAINT FK_184D34F9F675F31B');
        $this->addSql('ALTER TABLE stone_tag_stone DROP CONSTRAINT FK_6F88D02C943144E2');
        $this->addSql('ALTER TABLE stone_tag_stone DROP CONSTRAINT FK_6F88D02C1582D292');
        $this->addSql('ALTER TABLE stone_vote DROP CONSTRAINT FK_18D7DD101582D292');
        $this->addSql('ALTER TABLE stone_vote DROP CONSTRAINT FK_18D7DD10F675F31B');
        $this->addSql('ALTER TABLE stone_vote DROP CONSTRAINT FK_18D7DD10F8697D13');
        $this->addSql('DROP TABLE post_code');
        $this->addSql('DROP TABLE stone');
        $this->addSql('DROP TABLE stone_stone');
        $this->addSql('DROP TABLE stone_comment');
        $this->addSql('DROP TABLE stone_location');
        $this->addSql('DROP TABLE stone_photo');
        $this->addSql('DROP TABLE stone_post');
        $this->addSql('DROP TABLE stone_tag');
        $this->addSql('DROP TABLE stone_tag_stone');
        $this->addSql('DROP TABLE stone_vote');
        $this->addSql('DROP TABLE "user"');
    }
}
