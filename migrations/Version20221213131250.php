<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221213131250 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE stone_photo ALTER page TYPE BIGINT');
        $this->addSql('ALTER TABLE stone_post ALTER page TYPE BIGINT');
        $this->addSql('ALTER TABLE stone_post ALTER page DROP NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE stone_post ALTER page TYPE INT');
        $this->addSql('ALTER TABLE stone_post ALTER page SET NOT NULL');
        $this->addSql('ALTER TABLE stone_photo ALTER page TYPE INT');
    }
}
