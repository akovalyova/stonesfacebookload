<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230307114646 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE stone ALTER original DROP NOT NULL');
        $this->addSql('ALTER TABLE "user" ADD access_token TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE "user" ADD issued_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE "user" ADD profile_picture_size INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE stone ALTER original SET NOT NULL');
        $this->addSql('ALTER TABLE "user" DROP access_token');
        $this->addSql('ALTER TABLE "user" DROP issued_at');
        $this->addSql('ALTER TABLE "user" DROP profile_picture_size');
    }
}
