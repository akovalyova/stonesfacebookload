<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221213133157 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE stone_photo ALTER path TYPE TEXT');
        $this->addSql('ALTER TABLE stone_post ALTER post TYPE BIGINT');
        $this->addSql('ALTER TABLE stone_post ALTER post DROP NOT NULL');
        $this->addSql('ALTER TABLE "user" ALTER page TYPE BIGINT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE stone_photo ALTER path TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE stone_post ALTER post TYPE INT');
        $this->addSql('ALTER TABLE stone_post ALTER post SET NOT NULL');
        $this->addSql('ALTER TABLE "user" ALTER page TYPE INT');
    }
}
