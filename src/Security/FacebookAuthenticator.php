<?php
namespace App\Security;

use App\Entity\StonePhoto;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use KnpU\OAuth2ClientBundle\Security\Authenticator\OAuth2Authenticator;
use League\Flysystem\FilesystemException;
use League\Flysystem\FilesystemOperator;
use League\Flysystem\UnableToWriteFile;
use Safe\DateTime;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;

class FacebookAuthenticator extends OAuth2Authenticator implements AuthenticationEntrypointInterface
{

    public function __construct(private readonly EntityManagerInterface $entityManager,
                                private readonly UserPasswordHasherInterface $passwordHasher,
                                private readonly FilesystemOperator $defaultStorage,)
    {

    }

    public function supports(Request $request): ?bool
    {
        return $request->attributes->get('_route') === 'connect_facebook_check';
    }

    /**
     * @throws \JsonException
     */
    public function authenticate(Request $request): Passport
    {
        $signedRequest = $request->get('signedRequest');

        try {
            if ($signedRequest) {
                $appSecret = $_ENV['OAUTH_FACEBOOK_SECRET'];

                $userData['userID'] = $request->get('userID', 0);

                [$encodedSig, $payload] = explode('.', $signedRequest, 2);
                $sig = base64_decode(strtr($encodedSig, '-_', '+/'));
                $data = json_decode(base64_decode(strtr($payload, '-_', '+/')), true, 512, JSON_THROW_ON_ERROR);

                // Verify signature and extract data
                $expectedSig = hash_hmac('sha256', $payload, $appSecret, $raw = true);
                dump($data);

                if ($sig === $expectedSig && $userData['userID'] === $data['user_id']) {

                    $userData['userName'] = $request->get('userName', '');
                    $userData['email'] = $request->get('userEmail', '');
                    $userData['profilePicture'] = $request->get('userPicture', '');
                    $userData['profilePictureHeight'] = $request->get('userHeight', 0);
                    $accessToken = $request->get('accessToken', '');

                    $expirationTime = $request->get('expirationTime', null);
                    $userData['issuedAt']= isset($data['issued_at']) ? (new DateTime)->setTimestamp($data['issued_at']): null;
                    $userData['expiresAt'] = $expirationTime ? (new DateTime)->setTimestamp($expirationTime): null;

                    $names = explode(' ', $userData['userName']);
                    $nameParts = count($names);
                    $userData['firstName'] = $userData['lastName'] = $userData['middleName'] = '';
                    if ($nameParts === 3) {
                        [$userData['firstName'], $userData['middleName'], $userData['lastName']] = $names;
                    } elseif ($nameParts === 2) {
                        [$userData['firstName'], $userData['lastName']] = $names;
                    }

                    return new SelfValidatingPassport(
                        new UserBadge($accessToken, function () use ($accessToken, $userData) {
                            // 1) have they logged in with Facebook before? Easy!
                            $existingUser = $this->entityManager->getRepository(User::class)->findOneBy(['page' => $userData['userID']]);

                            if ($existingUser && $userData['email']) {
                                $existingUser->setEmail($userData['email']);

                                if ($userData['profilePicture']) {
                                    $existingUser->setProfilePicture($this->readProfileImage($userData['profilePicture'], $existingUser->getId()));
                                    $existingUser->setProfilePictureSize($userData['profilePictureHeight']);
                                }

                                $existingUser->setAccessToken($accessToken);
                                $existingUser->setExpiresAt($userData['expiresAt']);
                                $existingUser->setIssuedAt($userData['issuedAt']);
                                $this->entityManager->persist($existingUser);
                                $this->entityManager->flush();
                                return $existingUser;
                            }

                            // 2) do we have a matching user by email?
                            $user = $this->entityManager->getRepository(User::class)->findOneBy(['email' => $userData['email']]);

                            if ($user === null) {
                                // 3) Maybe you just want to "register" them by creating a User object
                                $user = new User();
                                $plaintextPassword = \bin2hex(\random_bytes(5));
                                $user = new User();
                                $user->setAccessToken($accessToken);
                                $user->setEmail($userData['email'])
                                    ->setLname($userData['lastName'])
                                    ->setFname($userData['firstName']);

                                $hashedPassword = $this->passwordHasher->hashPassword(
                                    $user,
                                    $plaintextPassword
                                );
                                $user->setPassword($hashedPassword);
                            }

                            $user->setExpiresAt($userData['expiresAt']);
                            $user->setIssuedAt($userData['issuedAt']);
                            $user->setPage($userData['userID']);
                            if ($userData['profilePicture']) {
                                $user->setProfilePictureSize($userData['profilePictureHeight']);
                            }
                            $this->entityManager->persist($user);
                            $this->entityManager->flush();

                            //download ,save image, set url to User
                            $user->setProfilePicture($this->readProfileImage($userData['profilePicture'], $user->getId()));
                            $this->entityManager->persist($user);
                            $this->entityManager->flush();

                            return $user;
                        })
                    );
                }
            }
        } catch (\Exception $e) {
            dump($e->getMessage());
        }

        return new Passport(
            new UserBadge('invalid_user', function ($userIdentifier) {
                return null;
            }),
            new PasswordCredentials('password')
        );

    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        // change "app_homepage" to some route in your app
        //$targetUrl = $this->router->generate('connect_facebook_success');

        //return new RedirectResponse($targetUrl);
        $user = $token->getUser();
        return  new JsonResponse([
            'email' => $user?->getEmail(),
            'kaminkyUserId'=> $user?->getId(),
            'name' => $user?->getName(),
            'createdAt' => $user?->getCreatedAt(),
            'profilePicture' => $user?->getProfilePicturePath()
        ]);
        // or, on success, let the request continue to be handled by the controller
        //return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $message = strtr($exception->getMessageKey(), $exception->getMessageData());

        return new Response($message, Response::HTTP_FORBIDDEN);
    }

    /**
     * Called when authentication is needed, but it's not sent.
     * This redirects to the 'login'.
     */
    public function start(Request $request, AuthenticationException $authException = null): Response
    {
        return new RedirectResponse(
            '/connect/facebook/', // might be the site, where users choose their oauth provider
            Response::HTTP_TEMPORARY_REDIRECT
        );
    }



// For some reason Curl through HTTPClient gives 403 error, thus doing it natively
    private function getFileContentsCurl(string $url) : string
    {
        $ch = \curl_init();

        \curl_setopt($ch, CURLOPT_URL, $url);

        \curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        \curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        $output = \curl_exec($ch);
        \curl_close($ch);

        if (!\is_bool($output)) {
            return $output;
        }

        return '';
    }


    private function readProfileImage($image, $userId): string
    {
        $imageFilepath = '';
        $imageContents = $this->getFileContentsCurl($image);
        if ($imageContents !== '' && $imageContents !== 'URL signature mismatch') {

            $imageName = 'profile_' . $userId . '.jpg';

            $imageFilepath = '/profiles/' . $imageName;
            try {
                $this->defaultStorage->write(
                    $imageFilepath,
                    $imageContents,
                );

            } catch (UnableToWriteFile $e) {
                $imageFilepath = '';
                dump($e->getMessage()); // TODO: change to logging
            } catch (FilesystemException $e) {
                $imageFilepath = '';
                dump($e->getMessage()); // TODO: change to logging
            }
        } else {
            dump('Trying to read image ' . $image . ' failed'); // TODO: change to logging
        }

        return $imageFilepath;
    }
}