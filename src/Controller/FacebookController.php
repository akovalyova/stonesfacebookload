<?php
declare(strict_types=1);

namespace App\Controller;

use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\SecurityBundle\Security;

class FacebookController extends AbstractController
{
    #[Route('/profile', name: 'profile')]
    public function connectProfileAction(Security $security, ClientRegistry $clientRegistry):JsonResponse
    {
        $user = $this->getUser();
        $email = $user?->getEmail();
        return  new JsonResponse(['email1' => $email]);
    }

    #[Route('/connect/authenticated', name: 'check_login')]
    public function connectCheckAction(Security $security, ClientRegistry $clientRegistry):JsonResponse
    {
        $user = $this->getUser();
        if ($user === null){
            return  new JsonResponse(null);
        }

        $email = $user?->getEmail();
        $name = $user?->getFname() . " ". $user?->getLname();
        $profilePicture = $user?->getProfilePicture();
        $userId = $user?->getId();
        $facebookId = $user?->getPage();

        return  new JsonResponse(['userId'=> $userId,
                                  'facebookId' => $facebookId,
                                  'email' => $email,
                                  'name' => $name,
                                  'profile_picture' => $profilePicture
        ]);
    }

    #[Route('/connect/success', name: 'connect_facebook_success')]
    public function connectSuccessAction(): JsonResponse
    {
        $user = $this->getUser();

        return  new JsonResponse([
            'email' => $user?->getEmail(),
            'userId'=> $user?->getId(),
            'name' => $user?->getName(),
            'createdAt' => $user?->getCreatedAt(),
            'profilePicture' => $user?->getProfilePicturePath()
        ]);
    }


    #[Route('/connect/facebook', name: 'connect_facebook_start')]
    public function connectAction(ClientRegistry $clientRegistry)
    {
        // will redirect to Facebook!
        return $clientRegistry
            ->getClient('facebook_main') // key used in config/packages/knpu_oauth2_client.yaml
            ->redirect([
                'public_profile',  'email' , 'user_birthday', 'user_gender'// the scopes you want to access
            ]);

      }


    #[Route('/logout', name: 'logout')]
    public function connectLogoutAction(Security $security, ClientRegistry $clientRegistry):JsonResponse
    {
        $logout = false;
        $user = $this->getUser();
        if ($user !== null) {
            $logout = true;
            $response = $security->logout(false);
        }

        $email = $user?->getEmail();
        return  new JsonResponse(['email1' => $email, 'logout' => $logout]);
    }

    #[Route('/connect/facebook/check', name: 'connect_facebook_check')]
    public function validateAccessTokenAction(Request $request, Security $security) : ?JsonResponse
    {

    }


}
