<?php

namespace App\Controller;

use App\Entity\StonePhoto;
use App\Repository\StoneRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;


#[AsController]
final class CreateStonePhotoAction extends AbstractController
{
    public function __construct(private readonly StoneRepository $stoneRepository)
    {

    }

    public function __invoke(Request $request): StonePhoto
    {
        $uploadedFile = $request->files->get('file');
        $stone_id = $request->get('stone_id');

        if (!$uploadedFile) {
            throw new BadRequestHttpException('"file" is required');
        }
        if (!$stone_id) {
            throw new BadRequestHttpException('"stone_id" is required');
        }
        if (($stone = $this->stoneRepository->find($stone_id)) === null)
        {
            throw new BadRequestHttpException('Stone is not found');
        }

        dump($uploadedFile);
        $originalName = $uploadedFile->getClientOriginalName();
        $mimeType = $uploadedFile->getClientMimeType();
        $fileSize = $uploadedFile->getMaxFilesize();

        $mediaObject = new StonePhoto();
        $mediaObject->file = $uploadedFile;
        $mediaObject->setMain(false);
        $mediaObject->setMimeType($mimeType);
        $mediaObject->setSize($fileSize);
        $mediaObject->setDisplayName($originalName);
        $mediaObject->setStone($stone);

        return $mediaObject;
    }
}