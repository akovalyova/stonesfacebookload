<?php

declare(strict_types=1);

namespace App\DataProvider;

use ApiPlatform\Doctrine\Orm\Paginator;
use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use Doctrine\ORM\Tools\Pagination\Paginator as DoctrinePaginator;
use App\Repository\UserRepository;
use Exception;

class UserCollectionDataProvider implements ProviderInterface
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @throws Exception
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []) : object | array | null
    {
        $page = $context['filters']['page'] ?? 1;
        if ($operation instanceof CollectionOperationInterface) {
            $doctrinePaginator = new DoctrinePaginator(
                $this->userRepository->findAllSortedByStonesCount((int) $page, $context['filters'])
            );

            return new Paginator($doctrinePaginator);

        }

        return $this->userRepository->find($uriVariables['id']);
    }
}
