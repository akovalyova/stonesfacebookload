<?php
declare(strict_types=1);

namespace App\Service\Strategy;

interface FacebookDataStrategyInterface
{
    public function getPosts(string $date = null): array;
}
