<?php
declare(strict_types=1);

namespace App\Service\Strategy;

use League\Flysystem\FilesystemOperator;
use Psr\Log\LoggerInterface;

class FacebookApiService extends AbstractFacebookService
{
    public function __construct(
        private readonly FilesystemOperator $defaultStorage,
        private readonly LoggerInterface $logger,
        private readonly string $accessToken,
        private readonly string $groupId,
        private readonly string $facebookUrl,
        private readonly string $userAgent,
        private readonly string $cookieFilePath,
        private readonly string $imageLocationPath,
    ) {
        parent::__construct($this->defaultStorage, $this->logger, $this->userAgent, $this->cookieFilePath, $this->imageLocationPath);
    }


    public function getPosts(string $date = null): array
    {
        try {
            $endpoint = sprintf(
                $this->facebookUrl . '?fields=id,message,created_time,from,likes.limit(0).summary(true),comments.limit(0).summary(true),shares,attachments{media,url}&limit=100&access_token=%s',
                $this->groupId,
                $this->accessToken
            );
            $processedPosts = [];
            $response = $this->getFileContentsCurl($endpoint);
            $data = json_decode($response, true, 512, JSON_THROW_ON_ERROR);

            if (isset($data['data']) && is_array($data['data'])) {
                foreach ($data['data'] as $post) {
                    $processedPost = [
                        'post_id' => $post['id'] ?? null,
                        'text' => $post['message'] ?? null,
                        'time' => $post['created_time'] ?? null,
                        'user_id' => $post['from']['id'] ?? null,
                        'username' => $post['from']['name'] ?? null,
                        'likes' => $post['likes']['summary']['total_count'] ?? null,
                        'comments' => $post['comments']['summary']['total_count'] ?? null,
                        'shares' => $post['shares']['count'] ?? null,
                        'image' => $post['attachments']['data'][0]['media']['image']['src'] ?? null,
                        'image_id' => $post['attachments']['data'][0]['media']['id'] ?? null,
                    ];

                    if (isset($processedPost['image'])) {
                        $imageDetails = $this->getPostImage($processedPost['image_id'], $processedPost['post_id'], $post['image']);
                        $processedPost = [...$processedPost, ...$imageDetails];
                    }

                    $processedPosts[] = $processedPost;
                }
            }
        } catch (\JsonException $je) {
            $this->logger->error("Error decoding JSON from Facebook API: " . $je->getMessage());
        } catch (\Exception $e) {
            $this->logger->error('Error fetching data from Facebook API: ' . $e->getMessage());
        }

        return $processedPosts;
    }
}
