<?php

declare(strict_types=1);

namespace App\Service\Strategy;

use League\Flysystem\FilesystemException;
use League\Flysystem\UnableToWriteFile;
use League\Flysystem\FilesystemOperator;
use Psr\Log\LoggerInterface;

abstract class AbstractFacebookService  implements FacebookDataStrategyInterface
{
    public function __construct(
        private readonly FilesystemOperator $defaultStorage,
        private readonly LoggerInterface $logger,
        private readonly string $userAgent,
        private readonly string $cookieFilePath,
        private readonly string $imageLocationPath,
    ) {
    }

    abstract public function getPosts(string $date = null): array;

    protected function getPostImage(string $image_id, string $post_id, string $url): array
    {
        $imageContents = $this->getFileContentsCurl($url);

        if (!$imageContents) {
            $this->logger->error("Failed to get image content for record ID: $post_id image $url");
            return [];
        }

        if ($imageContents === 'URL signature mismatch') {
            $this->logger->error("URL signature mismatch for record ID: $post_id image $url");
            return [];
        }

        $imagePrefix = $image_id && $url ? 'img_' : 'post_';
        $idValue = $image_id ?? $post_id;
        $imageName = $imagePrefix . $idValue . '.jpg';
        $imageFilepath = $this->imageLocationPath . $imageName;

        try {
            $this->defaultStorage->write($imageFilepath, $imageContents);
            $mime = $this->defaultStorage->mimeType($imageFilepath);
            $filesize = $this->defaultStorage->fileSize($imageFilepath);

            return [
                'image_mime' => $mime,
                'image_filesize' => $filesize,
                'image_path' => $imageFilepath,
                'image_name' => $imageName
            ];
        } catch (UnableToWriteFile|FilesystemException $e) {
            $this->logger->error("Could not write image " . $e->getMessage());
        }

        return [];
    }

    protected function getFileContentsCurl(string $url): string
    {
        try {
            $ch = \curl_init();
            \curl_setopt($ch, CURLOPT_URL, $url);
            \curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            \curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            if ($this->userAgent) {
                \curl_setopt($ch, CURLOPT_USERAGENT, $this->userAgent);
            }
            if ($this->cookieFilePath) {
                \curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookieFilePath);
            }
            $output = \curl_exec($ch);
            \curl_close($ch);
        } catch (\Exception $e) {
            $this->logger->error('Error fetching contents from URL: ' . $e->getMessage());
        }
        return is_string($output) ? $output : '';
    }
}
