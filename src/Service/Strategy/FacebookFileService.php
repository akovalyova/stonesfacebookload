<?php
declare(strict_types=1);

namespace App\Service\Strategy;

use League\Csv\Reader;
use League\Flysystem\FilesystemOperator;
use Psr\Log\LoggerInterface;
use Symfony\Component\Finder\Finder;

class FacebookFileService extends AbstractFacebookService
{

    public function __construct(private readonly FilesystemOperator $defaultStorage,
                                 private readonly LoggerInterface $logger,
                                 private readonly string $userAgent,
                                 private readonly string $cookieFilePath,
                                 private readonly string $imageLocationPath,
                                 private readonly string $facebookFilesLocationPath,
    )
    {
        parent::__construct($this->defaultStorage, $this->logger, $this->userAgent, $this->cookieFilePath, $this->imageLocationPath);
    }

    public function getPosts(string $date = null): array
    {
        $date = $date ?? date('Y-m-d');

        $finder = new Finder();
        $finder->in($this->facebookFilesLocationPath);
        $finder->sortByModifiedTime();
        $finder->reverseSorting();
        $finder->files()->name("kam_$date*.csv");

        $posts = [];
        $totalIndex = 0;

        foreach ($finder as $file) {
            try {
                $absoluteFilePath = $file->getRealPath();
                $reader = Reader::createFromPath($absoluteFilePath);
                $reader->setHeaderOffset(0);
                $newPosts = iterator_to_array($reader->getRecords());

                echo $absoluteFilePath . ' ' . count($newPosts) . "\n";
                $this->logger->info("Processing " . $absoluteFilePath . count($newPosts));

                foreach ($newPosts as $key => $record) {
                    $posts[$totalIndex] = $record;
                    if ($record['image']) {
                        $posts[$totalIndex]['image_id'] = $record['image_id'];
                        $imageDetails = $this->getPostImage($record);
                        $processedPost = [...$posts, ...$imageDetails];
                        $posts[$totalIndex] = $processedPost;
                    }
                    $totalIndex++;
                }
            } catch (\Exception $e) {
                $this->logger->error("Error processing file $absoluteFilePath :" . $e->getMessage());
            }
        }

        return $posts;
    }
}
