<?php
declare(strict_types=1);

namespace App\Service;

use App\Service\Strategy\FacebookDataStrategyInterface;

class FacebookStrategyService
{
    public function __construct(private readonly FacebookDataStrategyInterface $strategy)
    {

    }

    public function getPosts(string $date = null): array
    {
        return $this->strategy->getPosts($date);
    }
}
