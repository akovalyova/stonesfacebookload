<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Stone;
use App\Entity\User;
use App\Repository\StoneRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

class StoneService
{

    public function __construct(private readonly EntityManagerInterface $em,
                                private readonly StoneRepository $stoneRepository,
                                private readonly StonePhotoService $stonePhotoService,
                                private readonly StoneLocationService $stoneLocationService)
    {

    }

    public function createStone(array $record, User $user): Stone
    {
        $stone = $this->createStoneFromRecord($record, $user); // This can be the logic you provided before.

        if (isset($record['image_name'])) {
            $this->stonePhotoService->saveStonePhoto($stone, $record);
        }

        $this->stoneLocationService->createAndSaveLocationForStone($stone, $record);


        /*

                        //TODO: find existing tag for Postcode

                        $stoneTag = (new StoneTagStone())
                            ->setStone($stone)
                            ->setTag();
                        */

        // text -> tags
        // text --> postcode,
        // images_description ->tags

        return $stone;
    }

    public function createStoneFromRecord(array $record, User $user): Stone
    {
        $stone = new Stone();
        $stone->setAuthor($user)
            ->setDetails($record['text']);

        if (\preg_match_all('/\D*(\d{3}\s?\d{2})\D*/', $record['text'], $codeMatches)
            && isset($codeMatches[1][0])) {
            $postCode = $codeMatches[1][0];
            $postCode = \str_replace(' ', '', $postCode);
            $stone->setPostCode($postCode);
        }

        // FB post data
        $postDate = (new \DateTime())->setTimestamp((int) $record['timestamp']);
        $record['post_url'] = \str_replace('m.facebook.com', 'www.facebook.com', $record['post_url']);
        $image = $record['image']; // TODO: you mentioned saving high quality and low quality, you can add that logic here as well

        $stone->setFbPost($record['post_id'])
            ->setFbContent($record['text'])
            ->setPublishedAt($postDate)
            ->setFbComments((int) $record['comments'])
            ->setFbCommentsDetails($record['comments_full'])
            ->setFbUrl($record['post_url'])
            ->setFbLikes((int) $record['likes'])
            ->setFbPage($record['page_id'])
            ->setFbImagesString($record['images'])
            ->setFbImage($image);

        $this->em->persist($stone);
        $this->em->flush();

        return $stone;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function isStoneExistsFromPost(string $postId): bool
    {
        return $this->stoneRepository->findOneByPost($postId) !== null;
    }
}
