<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserService
{
    private array $skippedUsers = ['100000044686299', '100000131576773', '100001259959479'];

    public function __construct(private readonly EntityManagerInterface $em,
                                private readonly UserRepository $userRepository,
                                private readonly UserPasswordHasherInterface $passwordHasher)
    {

    }

    public function shouldSkipAdmins($facebookId): bool
    {
       return \in_array($facebookId, $this->skippedUsers, true);
    }

    public function getOrCreateUser(array $record): User
    {
        $email = $record['user_id'] . '@fakegmail.com';
        $user = $this->userRepository->findOneByEmail($email);

        if ($user === null) {
            $nameMatches = explode(' ', $record['username']);
            $fname = $lname = $mname = '';
            $nCnt = count($nameMatches);

            if ($nCnt === 3) {
                [$fname, $mname, $lname] = $nameMatches;
            } elseif ($nCnt === 2) {
                [$fname, $lname] = $nameMatches;
            }

            $plaintextPassword = bin2hex(random_bytes(5));
            $user = new User();
            $user->setPage($record['user_id'])
                ->setEmail($email)
                ->setLname($lname)
                ->setFname($fname)
                ->setMname($mname);

            $hashedPassword = $this->passwordHasher->hashPassword($user, $plaintextPassword);
            $user->setPassword($hashedPassword);
            $this->em->persist($user);
            $this->em->flush();
        }

        return $user;
    }

}
