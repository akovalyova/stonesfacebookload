<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Stone;
use App\Entity\StonePhoto;
use Doctrine\ORM\EntityManagerInterface;

class StonePhotoService
{

    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function saveStonePhoto(Stone $stone, array $record): void
    {
        $stonePhoto = (new StonePhoto())
            ->setStone($stone)
            ->setDisplayName($record['image_name'])
            ->setSize($record['image_filesize'])
            ->setMimeType($record['image_mime'])
            ->setPath($record['image_path'])
            ->setMain(false);

        if ($record['image_id']) {
            $stonePhoto->setPage($record['image_id']);
        }

        $this->em->persist($stonePhoto);
        $this->em->flush();
    }
}
