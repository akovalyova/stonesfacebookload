<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Stone;
use App\Entity\StoneLocation;
use Doctrine\ORM\EntityManagerInterface;
use Geocoder\Query\GeocodeQuery;
use Geocoder\Provider\GoogleMaps\GoogleMaps;
use Psr\Log\LoggerInterface;

class StoneLocationService
{
    public function __construct(private readonly EntityManagerInterface $em,
                                private readonly GoogleMaps $googleMapsProvider,
                                private readonly LoggerInterface $logger)
    {
    }

    /**
     * @throws \JsonException
     */
    public function createAndSaveLocationForStone(Stone $stone, array $record): void
    {
        $geoLocation = $this->getGeoLocationFromRecordText($record['text']);

        if ($geoLocation) {
            $firstLocation = $geoLocation->first(); // Sometimes correct one can be in the second element or further
            $locationArray = $firstLocation->toArray();
            $coordinates = $firstLocation->getCoordinates();
            $postDate = (new \DateTime())->setTimestamp((int) $record['timestamp']);

            $stoneLocation = (new StoneLocation())
                ->setStone($stone)
                ->setGeocodeDetails($locationArray)
                ->setLocationName($firstLocation->getFormattedAddress())
                ->setLat((string) $coordinates?->getLatitude())
                ->setLong((string) $coordinates?->getLongitude())
                ->setDateLocated($postDate);

            $this->em->persist($stoneLocation);
            $this->em->flush();
        }
    }

    private function getGeoLocationFromRecordText(string $text): ?\Geocoder\Model\AddressCollection
    {
        $cleanText = \preg_replace('/(\d{3}\s?\d{2})/', ' ', $text);
        $lines = \explode("\n", $cleanText);
        foreach ($lines as $line) {
            if (\preg_match('/čekal|nález|nálezy|nalezen|nalezeno|nalezeny|nalezené|nalezený|našel|našla|našly|našli|nasli|nasly|nasel|nasla/umi', $line)) {
                try {
                    return $this->googleMapsProvider->geocodeQuery(GeocodeQuery::create($line));
                }  catch (\Geocoder\Exception\Exception $e) {
                    $this->logger->error("Error occurred while trying to geocode location" . $line . " " . $e->getMessage());
                }
            }
        }

        return null;
    }
}
