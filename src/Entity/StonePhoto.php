<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\OpenApi\Model;
use App\Controller\CreateStonePhotoAction;
use App\Repository\StonePhotoRepository;
use Doctrine\DBAL\Types\{Types};
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\{Annotation as Vich};
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;


#[Vich\Uploadable]
#[ORM\Entity(repositoryClass: StonePhotoRepository::class)]
#[ApiResource(
    types: ['https://schema.org/MediaObject'],
    operations: [
        new Get(),
        new GetCollection(),
        new Post(controller: CreateStonePhotoAction::class,
            validationContext: ['groups' => ['Default', 'photo_object_create']],
            deserialize: false
        )
    ],
    normalizationContext: ['groups' => ['photo_object:read']]
)]
class StonePhoto
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['user_details:read', 'stone_details:read','location_details:read'])]
    private ?int $id = null;

    #[ORM\Column(length: 100, nullable: true)]
    #[Groups(['user_details:read', 'stone_details:read','location_details:read'])]
    private ?string $name = null;

    #[ORM\Column(length: 100, nullable: true)]
    #[Groups(['user_details:read', 'stone_details:read','location_details:read'])]
    private ?string $displayName = null;


    #[Vich\UploadableField(mapping: "photo_object", fileNameProperty: "path")]
    #[Assert\NotNull(groups: ['photo_object_create'])]
    public ?File $file = null;


    #[ORM\ManyToOne(targetEntity: Stone::class, inversedBy: 'photos')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Stone $stone = null;

    #[ORM\Column(type: Types::TEXT, nullable: false)]
    #[Groups(['user_details:read', 'stone_details:read','location_details:read'])]
    private ?string $path = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['user_details:read', 'stone_details:read','location_details:read'])]
    private ?int $size = null;

    #[ORM\Column(length: 127, nullable: true)]
    #[Groups(['user_details:read', 'stone_details:read','location_details:read'])]
    private ?string $mimeType = null;

    #[ORM\Column]
    #[Groups(['user_details:read', 'stone_details:read','location_details:read'])]
    private ?bool $main = null;

    #[ORM\Column(type: Types::BIGINT, nullable: true)]
    #[Groups(['user_details:read', 'stone_details:read','location_details:read'])]
    private ?string $page = null;

    #[ORM\Column(type: 'datetime', options: ['default' => 'CURRENT_TIMESTAMP'])]
    private ?\DateTime $createdAt = null;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId() : ?int
    {
        return $this->id;
    }

    public function getName() : ?string
    {
        return $this->name;
    }

    public function setName(?string $name) : self
    {
        $this->name = $name;

        return $this;
    }

    public function getDisplayName() : ?string
    {
        return $this->displayName;
    }

    public function setDisplayName(string $displayName) : self
    {
        $this->displayName = $displayName;

        return $this;
    }


    public function getStone() : ?Stone
    {
        return $this->stone;
    }

    public function setStone(?Stone $stone) : self
    {
        $this->stone = $stone;

        return $this;
    }

    #[ApiProperty(types: ['https://schema.org/contentUrl'])]
    #[Groups(['photo_object:read', 'user_details:read', 'stone_details:read', 'location_details:read'])]
    public function getPathFull() : ?string
    {
        return $this->path ? $_ENV['IMAGES_URL'].'kaminky/'.$this->path : null;
    }

    public function getPath() : ?string
    {
        return $this->path;
    }

    public function setPath(string $path) : self
    {
        $this->path = $path;

        return $this;
    }

    public function getSize() : ?int
    {
        return $this->size;
    }

    public function setSize(int $size) : self
    {
        $this->size = $size;

        return $this;
    }

    public function getMimeType() : ?string
    {
        return $this->mimeType;
    }

    public function setMimeType(?string $mimeType) : self
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    public function isMain() : ?bool
    {
        return $this->main;
    }

    public function setMain(bool $main) : self
    {
        $this->main = $main;

        return $this;
    }

    public function getPage() : ?string
    {
        return $this->page;
    }

    public function setPage(?string $page) : self
    {
        $this->page = $page;

        return $this;
    }

    public function getCreatedAt() : ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt) : self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
