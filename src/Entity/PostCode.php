<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\PostCodeRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PostCodeRepository::class)]
class PostCode
{
    #[ORM\Id]
    #[ORM\Column(length: 5, unique: true)]
    private string $code;

    #[ORM\Column(length: 255)]
    private string $postName;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $regionName = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $townName = null;

    public function getCode() : string
    {
        return $this->code;
    }

    public function setCode(string $code) : self
    {
        $this->code = $code;

        return $this;
    }

    public function getPostName() : string
    {
        return $this->postName;
    }

    public function setPostName(string $postName) : self
    {
        $this->postName = $postName;

        return $this;
    }

    public function getRegionName() : ?string
    {
        return $this->regionName;
    }

    public function setRegionName(string $regionName) : self
    {
        $this->regionName = $regionName;

        return $this;
    }

    public function getTownName() : ?string
    {
        return $this->townName;
    }

    public function setTownName(string $townName) : self
    {
        $this->townName = $townName;

        return $this;
    }
}
