<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\StoneVoteRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: StoneVoteRepository::class)]
class StoneVote
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: Stone::class, inversedBy: 'votes')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Stone $stone = null;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'votes')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $author = null;

    #[ORM\OneToOne(targetEntity: StoneComment::class, cascade: ['persist', 'remove'])]
    private ?StoneComment $comment = null;

    #[ORM\Column(type: 'datetime', options: ['default' => 'CURRENT_TIMESTAMP'])]
    private ?\DateTime $createdAt = null;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId() : ?int
    {
        return $this->id;
    }

    public function getStone() : ?Stone
    {
        return $this->stone;
    }

    public function setStone(?Stone $stone) : self
    {
        $this->stone = $stone;

        return $this;
    }

    public function getAuthor() : ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author) : self
    {
        $this->author = $author;

        return $this;
    }

    public function getComment() : ?StoneComment
    {
        return $this->comment;
    }

    public function setComment(?StoneComment $comment) : self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getCreatedAt() : ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt) : self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
