<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\StoneTagRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: StoneTagRepository::class)]
class StoneTag
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private ?string $name = null;

    /** @var Collection<int,Stone> */
    #[ORM\ManyToMany(targetEntity: Stone::class, inversedBy: 'tags')]
    private Collection $stones;

    public function __construct()
    {
        $this->stones = new ArrayCollection();
    }

    public function getId() : ?int
    {
        return $this->id;
    }

    public function getName() : ?string
    {
        return $this->name;
    }

    public function setName(string $name) : self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Stone>
     */
    public function getStone() : Collection
    {
        return $this->stones;
    }

    public function addStone(Stone $stone) : self
    {
        if (!$this->stones->contains($stone)) {
            $this->stones->add($stone);
        }

        return $this;
    }

    public function removeStone(Stone $stone) : self
    {
        $this->stones->removeElement($stone);

        return $this;
    }
}
