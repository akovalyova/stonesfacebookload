<?php

declare(strict_types=1);

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use App\Repository\StoneRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: StoneRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(),
        new Get(),
        new Post()
    ],
    normalizationContext: ['groups' => ['stone_details:read']],
    denormalizationContext: ['groups' => ['stone:write']],
    order: ['createdAt' => 'DESC'],
    paginationItemsPerPage: 50
)]
#[ApiFilter(OrderFilter::class, properties: ['createdAt' => 'DESC', 'publishedAt' => 'DESC', 'fbLikes' => 'DESC', 'default_direction' => 'DESC'])]
#[ApiFilter(SearchFilter::class, properties: ['author.id' => 'exact',  'postCode' => 'exact', 'user.lname' => 'partial', 'details' => 'partial'])]
#[ApiFilter(DateFilter::class, properties: ['publishedAt'])]
#[ApiFilter(BooleanFilter::class, properties: ['original', 'withAuthor'])]


class Stone
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['stone:write', 'stone_details:read', 'stone_details:read', 'user_details:read', 'location_details:read'])]
    private ?int $id = null;

    #[ORM\Column(length: 5, nullable: true)]
    #[Groups(['stone_details:read',  'user_details:read','location_details:read', 'stone:write'])]
    private ?string $postCode = null;

    #[ORM\Column(length: 60, nullable: true)]
    #[Groups(['user_details:read', 'stone:write'])]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Assert\Type('string')]
    #[Groups(['stone_details:read', 'user_details:read', 'location_details:read', 'stone:write'])]
    private ?string $details = null;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'stones')]
    #[Groups(['stone_details:read:withAuthor', 'location_details:read', 'stone:write'])]
    private ?User $author = null;

    /** @var Collection<int, StonePhoto> */
    #[ORM\OneToMany(mappedBy: 'stone', targetEntity: StonePhoto::class, orphanRemoval: true)]
    #[Groups(['user_details:read', 'stone_details:read', 'location_details:read'])]
    private Collection $photos;

    /** @var Collection<int, Stone> */
    #[ORM\ManyToMany(targetEntity: self::class, inversedBy: 'similarStones')]
    private Collection $similarity;

    /** @var Collection<int, Stone> */
    #[ORM\ManyToMany(targetEntity: self::class, mappedBy: 'similarity')]
    #[Groups(['stone_details:read'])]
    private Collection $similarStones;

    /** @var Collection<int, StoneTag> */
    #[ORM\ManyToMany(targetEntity: StoneTag::class, mappedBy: 'stones')]
    #[Groups(['stone_details:read', 'user_details:read'])]
    private Collection $tags;

    /** @var Collection<int, StoneVote> */
    #[ORM\OneToMany(mappedBy: 'stone', targetEntity: StoneVote::class)]
    #[Groups(['stone_details:read', 'user_details:read'])]
    private Collection $votes;

    #[ORM\Column(nullable: true)]
    #[Assert\Type('boolean')]
    #[Groups(['stone_details:read', 'user_details:read', 'stone:write'])]
    private bool $original = false;

    /** @var Collection<int, StoneLocation> */
    #[ORM\OneToMany(mappedBy: 'stone', targetEntity: StoneLocation::class, cascade:  ['persist'])]
    #[Groups(['user_details:read',  'stone_details:read', 'stone:write'])]
    private Collection $stoneLocations;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'children')]
    private ?self $parent = null;

    /** @var Collection<int, Stone> */
    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: self::class)]
    private Collection $children;

    #[ORM\Column(type: 'datetime', options: ['default' => 'CURRENT_TIMESTAMP'])]
    #[Assert\Type('DateTime')]
    #[Groups(['stone_details:read', 'user_details:read', 'stone:write'])]
    private ?\DateTime $createdAt = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['user_details:read', 'stone_details:read', 'location_details:read'])]
    private ?string $fbUrl = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['user_details:read', 'stone_details:read', 'location_details:read'])]
    private ?int $fbLikes = 0;

    #[ORM\Column(nullable: true)]
    #[Groups(['user_details:read', 'stone_details:read', 'location_details:read'])]
    private ?int $fbComments = 0;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['user_details:read', 'stone_details:read', 'location_details:read'])]
    private ?string $fbContent = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['user_details:read', 'stone_details:read'])]
    private ?string $fbImagesString = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['user_details:read', 'stone_details:read'])]
    private ?string $fbCommentsDetails = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $fbImage = null;

    #[ORM\Column(type: Types::BIGINT, nullable: true)]
    #[Groups(['user_details:read', 'stone_details:read', 'location_details:read'])]
    private ?string $fbPage = null;

    #[ORM\Column(type: Types::BIGINT, nullable: true)]
    #[Groups('stone_details:read', 'location_details:read')]
    private ?string $fbPost = null;

    #[ORM\Column(type: 'datetime', options: ['default' => 'CURRENT_TIMESTAMP', 'location_details:read'])]
    #[Groups(['stone_details:read'])]
    private ?\DateTime $publishedAt = null;



    public function __construct()
    {
        $this->photos = new ArrayCollection();
        $this->similarity = new ArrayCollection();
        $this->similarStones = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->votes = new ArrayCollection();
        $this->stoneLocations = new ArrayCollection();
        $this->children = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->publishedAt = new \DateTime();
    }

    public function getId() : ?int
    {
        return $this->id;
    }

    public function getPostCode() : ?string
    {
        return $this->postCode;
    }

    public function setPostCode(string $postCode) : self
    {
        $this->postCode = $postCode;

        return $this;
    }

    public function getName() : ?string
    {
        return $this->name;
    }

    public function setName(string $name) : self
    {
        $this->name = $name;

        return $this;
    }

    public function getDetails() : ?string
    {
        return $this->details;
    }

    public function setDetails(?string $details) : self
    {
        $this->details = $details;

        return $this;
    }

    public function getAuthor() : ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author) : self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection<int, StonePhoto>
     */
    public function getPhotos() : Collection
    {
        return $this->photos;
    }

    public function addPhoto(StonePhoto $photo) : self
    {
        if (!$this->photos->contains($photo)) {
            $this->photos->add($photo);
            $photo->setStone($this);
        }

        return $this;
    }

    public function removePhoto(StonePhoto $photo) : self
    {
        if ($this->photos->removeElement($photo)) {
            // set the owning side to null (unless already changed)
            if ($photo->getStone() === $this) {
                $photo->setStone(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getSimilarity() : Collection
    {
        return $this->similarity;
    }

    public function addSimilarity(self $similarity) : self
    {
        if (!$this->similarity->contains($similarity)) {
            $this->similarity->add($similarity);
        }

        return $this;
    }

    public function removeSimilarity(self $similarity) : self
    {
        $this->similarity->removeElement($similarity);

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getSimilarStones() : Collection
    {
        return $this->similarStones;
    }

    public function addSimilarStone(self $similarStone) : self
    {
        if (!$this->similarStones->contains($similarStone)) {
            $this->similarStones->add($similarStone);
            $similarStone->addSimilarity($this);
        }

        return $this;
    }

    public function removeSimilarStone(self $similarStone) : self
    {
        if ($this->similarStones->removeElement($similarStone)) {
            $similarStone->removeSimilarity($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, StoneTag>
     */
    public function getTags() : Collection
    {
        return $this->tags;
    }

    public function addTag(StoneTag $tag) : self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags->add($tag);
            $tag->addStone($this);
        }

        return $this;
    }

    public function removeTag(StoneTag $tag) : self
    {
        if ($this->tags->removeElement($tag)) {
            $tag->removeStone($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, StoneVote>
     */
    public function getVotes() : Collection
    {
        return $this->votes;
    }

    public function addVote(StoneVote $vote) : self
    {
        if (!$this->votes->contains($vote)) {
            $this->votes->add($vote);
            $vote->setStone($this);
        }

        return $this;
    }

    public function removeVote(StoneVote $vote) : self
    {
        if ($this->votes->removeElement($vote)) {
            // set the owning side to null (unless already changed)
            if ($vote->getStone() === $this) {
                $vote->setStone(null);
            }
        }

        return $this;
    }

    public function isOriginal() : bool
    {
        return $this->original;
    }

    public function setOriginal(bool $original) : self
    {
        $this->original = $original;

        return $this;
    }

    /**
     * @return Collection<int, StoneLocation>
     */
    public function getStoneLocations() : Collection
    {
        return $this->stoneLocations;
    }

    public function addStoneLocation(StoneLocation $stoneLocation) : self
    {
        if (!$this->stoneLocations->contains($stoneLocation)) {
            $this->stoneLocations->add($stoneLocation);
            $stoneLocation->setStone($this);
        }

        return $this;
    }

    public function removeStoneLocation(StoneLocation $stoneLocation) : self
    {
        if ($this->stoneLocations->removeElement($stoneLocation)) {
            // set the owning side to null (unless already changed)
            if ($stoneLocation->getStone() === $this) {
                $stoneLocation->setStone(null);
            }
        }

        return $this;
    }

    public function getParent() : ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent) : self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getChildren() : Collection
    {
        return $this->children;
    }

    public function addChild(self $child) : self
    {
        if (!$this->children->contains($child)) {
            $this->children->add($child);
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(self $child) : self
    {
        if ($this->children->removeElement($child)) {
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }


    public function getCreatedAt() : ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt) : self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    #[Groups(['stone_details:read', 'user_details:read', 'location_details:read'])]
    public function getCreatedAtFormatted() : ?string
    {
        return $this->createdAt?->format('d-m-Y');
    }

    public function getFbUrl() : ?string
    {
        return $this->fbUrl;
    }

    public function setFbUrl(?string $fbUrl) : self
    {
        $this->fbUrl = $fbUrl;

        return $this;
    }

    public function getFbPage() : ?string
    {
        return $this->fbPage;
    }

    public function setFbPage(?string $fbPage) : self
    {
        $this->fbPage = $fbPage;

        return $this;
    }

    public function getFbLikes() : ?int
    {
        return $this->fbLikes;
    }

    public function setFbLikes(?int $fbLikes) : self
    {
        $this->fbLikes = $fbLikes;

        return $this;
    }

    public function getFbComments() : ?int
    {
        return $this->fbComments;
    }

    public function setFbComments(?int $FbComments) : self
    {
        $this->fbComments = $FbComments;

        return $this;
    }

    public function getFbContent() : ?string
    {
        return $this->fbContent;
    }

    public function setFbContent(?string $fbContent) : self
    {
        $this->fbContent = $fbContent;

        return $this;
    }


    public function getFbImagesString() : ?string
    {
        return $this->fbImagesString;
    }

    public function setFbImagesString(?string $fbImagesString) : self
    {
        $this->fbImagesString = $fbImagesString;

        return $this;
    }

    public function getFbCommentsDetails() : ?string
    {
        return $this->fbCommentsDetails;
    }

    public function setFbCommentsDetails(?string $fbCommentsDetails) : self
    {
        $this->fbCommentsDetails = $fbCommentsDetails;

        return $this;
    }


    public function getFbImage() : ?string
    {
        return $this->fbImage;
    }

    public function setFbImage(string $fbImage) : self
    {
        $this->fbImage = $fbImage;

        return $this;
    }

    public function getPublishedAt() : ?\DateTime
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(\DateTime $publishedAt) : self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    #[Groups(['user_details:read', 'stone_details:read'])]
    public function getPublishedAtFormatted() : ?string
    {
        return $this->publishedAt?->format('d-m-Y');
    }

    public function getFbPost() : ?string
    {
        return $this->fbPost;
    }

    public function setFbPost(?string $fbPost) : self
    {
        $this->fbPost = $fbPost;

        return $this;
    }
}
