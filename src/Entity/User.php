<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use App\DataProvider\UserCollectionDataProvider;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[ApiResource(
    normalizationContext: ['groups' => ['user:read', 'user_details:read'], 'id' => 'createdAt'],
    denormalizationContext: ['groups' => ['user:write']],
    order: ['id' => 'DESC'],
    paginationMaximumItemsPerPage: 100,
    provider: UserCollectionDataProvider::class,
)]
#[ApiFilter(SearchFilter::class, properties: ['lname' => 'partial'])]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['user:read', 'stone_details:read', 'location_details:read'])]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    #[Groups('user:read', 'location_details:read')]
    private ?string $email = null;

    /** @var array<string> */
    #[ORM\Column]
    private array $roles = [];

    #[ORM\Column]
    private ?string $password = null;

    /** @var Collection<int, Stone> */
    #[ORM\OneToMany(mappedBy: 'author', targetEntity: Stone::class)]
    private Collection $stones;

    /** @var Collection<int, StoneComment> */
    #[ORM\OneToMany(mappedBy: 'author', targetEntity: StoneComment::class)]
    #[Groups('user:read')]
    private Collection $comments;

    /** @var Collection<int, StoneVote> */
    #[ORM\OneToMany(mappedBy: 'author', targetEntity: StoneVote::class)]
    #[Groups('user:read')]
    private Collection $votes;

    #[ORM\Column(length: 50, nullable: true)]
    #[Groups(['user:read', 'stone_details:read', 'location_details:read'])]
    private ?string $fname = null;

    #[ORM\Column(length: 50, nullable: true)]
    #[Groups(['user:read', 'stone_details:read', 'location_details:read'])]
    private ?string $lname = null;

    #[ORM\Column(length: 50, nullable: true)]
    #[Groups(['user:read', 'stone_details:read', 'location_details:read'])]
    private ?string $mname = null;

    #[ORM\Column(type: Types::BIGINT, nullable: true)]
    #[Groups(['user:read', 'location_details:read'])]
    private ?string $page = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['user:read', 'stone_details:read', 'location_details:read'])]
    private ?string $profilePicture = null;

    #[ORM\Column(type: 'datetime', options: ['default' => 'CURRENT_TIMESTAMP'])]
    private ?\DateTime $createdAt = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $accessToken = null;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTime $issuedAt = null;

    #[ORM\Column(nullable: true)]
    private ?int $profilePictureSize = null;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTime $expiresAt = null;

    public function __construct()
    {
        $this->stones = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->votes = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    public function getId() : ?int
    {
        return $this->id;
    }

    public function getEmail() : ?string
    {
        return $this->email;
    }

    public function setEmail(string $email) : self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier() : string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles() : array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return \array_unique($roles);
    }

    /**
     * @param array<string> $roles
     *
     * @return $this
     */
    public function setRoles(array $roles) : self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword() : ?string
    {
        return $this->password;
    }

    public function setPassword(string $password) : self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials() : void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection<int, Stone>
     */
    public function getStones() : Collection
    {
        return $this->stones;
    }

    public function addStone(Stone $stone) : self
    {
        if (!$this->stones->contains($stone)) {
            $this->stones->add($stone);
            $stone->setAuthor($this);
        }

        return $this;
    }

    public function removeStone(Stone $stone) : self
    {
        if ($this->stones->removeElement($stone)) {
            // set the owning side to null (unless already changed)
            if ($stone->getAuthor() === $this) {
                $stone->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, StoneComment>
     */
    public function getComments() : Collection
    {
        return $this->comments;
    }

    public function addComment(StoneComment $comment) : self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments->add($comment);
            $comment->setAuthor($this);
        }

        return $this;
    }

    public function removeComment(StoneComment $comment) : self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getAuthor() === $this) {
                $comment->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, StoneVote>
     */
    public function getVotes() : Collection
    {
        return $this->votes;
    }

    public function addVote(StoneVote $vote) : self
    {
        if (!$this->votes->contains($vote)) {
            $this->votes->add($vote);
            $vote->setAuthor($this);
        }

        return $this;
    }

    public function removeVote(StoneVote $vote) : self
    {
        if ($this->votes->removeElement($vote)) {
            // set the owning side to null (unless already changed)
            if ($vote->getAuthor() === $this) {
                $vote->setAuthor(null);
            }
        }

        return $this;
    }

    public function getFname() : ?string
    {
        return $this->fname;
    }

    public function setFname(?string $fname) : self
    {
        $this->fname = $fname;

        return $this;
    }

    public function getName() : ?string
    {
        return $this->fname. ' '. $this->lname;
    }

    public function getLname() : ?string
    {
        return $this->lname;
    }

    public function setLname(?string $lname) : self
    {
        $this->lname = $lname;

        return $this;
    }

    public function getMname() : ?string
    {
        return $this->mname;
    }

    public function setMname(?string $mname) : self
    {
        $this->mname = $mname;

        return $this;
    }

    public function getPage() : ?string
    {
        return $this->page;
    }

    public function setPage(?string $page) : self
    {
        $this->page = $page;

        return $this;
    }


    public function getProfilePicture() : ?string
    {
        return $this->profilePicture;
    }

    public function setProfilePicture(?string $profilePicture) : self
    {
        $this->profilePicture = $profilePicture;

        return $this;
    }

    #[Groups(['user:read', 'stone_details:read', 'location_details:read'])]
    public function getProfilePicturePath() : ?string
    {
        return $this->profilePicture ? $_ENV['IMAGES_URL'].$this->profilePicture : null;
    }



    public function __toString() : string
    {
        return (string) $this->id;
    }

    public function getCreatedAt() : ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt) : self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getAccessToken(): ?string
    {
        return $this->accessToken;
    }

    public function setAccessToken(?string $accessToken): self
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    public function getIssuedAt(): ?\DateTime
    {
        return $this->issuedAt;
    }

    public function setIssuedAt(?\DateTime $issuedAt): self
    {
        $this->issuedAt = $issuedAt;

        return $this;
    }

    public function getProfilePictureSize(): ?int
    {
        return $this->profilePictureSize;
    }

    public function setProfilePictureSize(?int $profilePictureSize): self
    {
        $this->profilePictureSize = $profilePictureSize;

        return $this;
    }

    public function getExpiresAt(): ?\DateTimeInterface
    {
        return $this->expiresAt;
    }

    public function setExpiresAt(?\DateTime $expiresAt): self
    {
        $this->expiresAt = $expiresAt;

        return $this;
    }
}
