<?php

declare(strict_types=1);

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\StoneLocationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: StoneLocationRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['location_details:read']],
    denormalizationContext: ['groups' => ['location:write']],
    order: ['dateLocated' => 'DESC'],
    paginationItemsPerPage: 50
)]
#[ApiFilter(BooleanFilter::class, properties: ['stone.original'])]
#[ApiFilter(OrderFilter::class, properties: ['stone.publishedAt' => 'DESC', 'stone.fbLikes' => 'DESC', 'default_direction' => 'DESC'])]
#[ApiFilter(SearchFilter::class, properties: [ 'stone.postCode' => 'exact', 'stone.details' => 'partial'])]
class StoneLocation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['user_details:read',  'stone_details:read', 'location_details:read'])]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Assert\NotBlank]
    #[Assert\Type('string')]
    #[Groups(['stone:write', 'user_details:read',  'stone_details:read', 'location_details:read'])]
    private ?string $locationName = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 11, scale: 8, nullable: true)]
    #[Assert\NotBlank]
    #[Assert\Type('string')]
    #[Groups(['stone:write', 'user_details:read',  'stone_details:read','location_details:read'])]
    private ?string $long = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 8, nullable: true)]
    #[Assert\NotBlank]
    #[Assert\Type('string')]
    #[Groups(['stone:write', 'user_details:read',  'stone_details:read', 'location_details:read'])]
    private ?string $lat = null;

    #[Groups(['stone:write'])]
    #[Assert\Type('string')]
    #[ORM\Column(length: 255, nullable: true)]
    private ?string $details = null;

    #[ORM\ManyToOne(targetEntity: Stone::class, inversedBy: 'stoneLocations')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['location_details:read'])]
    private ?Stone $stone = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    #[Assert\Type('DateTime')]
    #[Groups(['stone:write', 'user_details:read',  'stone_details:read', 'location_details:read'])]
    private ?\DateTime $dateLocated = null;

    #[ORM\Column(type: 'json')]
    #[Assert\Json]
    #[Groups(['stone:write', 'stone_details:read'])]
    private mixed $geocodeDetails = [];

    public function __construct()
    {
        $this->dateLocated = new \DateTime();
    }

    public function getId() : ?int
    {
        return $this->id;
    }

    public function getLocationName() : ?string
    {
        return $this->locationName;
    }

    public function setLocationName(?string $locationName) : self
    {
        $this->locationName = $locationName;

        return $this;
    }

    public function getLong() : ?string
    {
        return $this->long;
    }

    public function setLong(string $long) : self
    {
        $this->long = $long;

        return $this;
    }

    public function getLat() : ?string
    {
        return $this->lat;
    }

    public function setLat(string $lat) : self
    {
        $this->lat = $lat;

        return $this;
    }

    public function getDetails() : ?string
    {
        return $this->details;
    }

    public function setDetails(?string $details) : self
    {
        $this->details = $details;

        return $this;
    }

    public function getStone() : ?Stone
    {
        return $this->stone;
    }

    public function setStone(?Stone $stone) : self
    {
        $this->stone = $stone;

        return $this;
    }

    #[Groups(['user_details:read',  'stone_details:read', 'location_details:read'])]
    public function getDateLocatedFormatted() : ?string
    {
        if ($this->dateLocated) {
            return $this->dateLocated->format('d-m-Y');
        }

        return null;
    }

    #[Groups(['user_details:read',  'stone_details:read'])]
    public function getMapCenter() : array
    {
        $lat = $this->lat;
        $lng = $this->long;
        if (isset($this->geocodeDetails['bounds'])) {
            $lat = ($this->geocodeDetails['bounds']['south'] + $this->geocodeDetails['bounds']['north']) / 2;
            $lng = ($this->geocodeDetails['bounds']['east'] + $this->geocodeDetails['bounds']['west']) / 2;
        }
        return ['lat' => $lat, 'lng' => $lng];
    }

    public function getDateLocated() : ?\DateTime
    {
        return $this->dateLocated;
    }

    public function setDateLocated(\DateTime $dateLocated) : self
    {
        $this->dateLocated = $dateLocated;

        return $this;
    }

    public function getGeocodeDetails() : mixed
    {
        return $this->geocodeDetails;
    }

    /**
     * @throws \JsonException
     */
    public function setGeocodeDetails(mixed $geocodeDetails) : self
    {

        if(isset($geocodeDetails[0]))
        {
            $geocodeDetails = $geocodeDetails[0];
            $this->geocodeDetails = json_decode($geocodeDetails, true, 512, JSON_THROW_ON_ERROR);
        } else {
            $this->geocodeDetails = $geocodeDetails;
        }

        return $this;
    }
}
