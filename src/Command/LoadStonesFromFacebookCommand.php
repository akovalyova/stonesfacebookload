<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\FacebookStrategyService;
use App\Service\StoneService;
use App\Service\UserService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(name: 'app:load-facebook')]
class LoadStonesFromFacebookCommand extends Command
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ManagerRegistry        $managerRegistry,
        private readonly LoggerInterface        $logger,
        private readonly UserService            $userService,
        private readonly StoneService           $stoneService,
        private readonly FacebookStrategyService   $facebookService,
        private readonly string $userAgent,
    ) {
        parent::__construct();

        ini_set('user_agent', $this->userAgent);
    }

    protected function configure(): void
    {
        $this->addOption('date', 'd', InputOption::VALUE_OPTIONAL, 'The date for which to load posts, format: YYYY-MM-DD');
    }

    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $date = $input->getOption('date') ?? date('Y-m-d');

        $io = new SymfonyStyle($input, $output);
        $io->title('Attempting import of Feed...');
        $this->logger->info('Attempting import of Feed...');


        $facebookPosts = $this->facebookService->getPosts($date);

        $errorMessages = [];
        $stonesError = $stonesProcessed = $stonesIgnored = 0;

        foreach ($facebookPosts as $offset => $record) {
            try {
                $io->title($offset . " " .$record['post_id'].":\n");

                if ($this->userService->shouldSkipAdmins($record['user_id'])) {
                    $stonesIgnored++;
                    $io->warning("Admin post ignored\n");
                } elseif (!$this->stoneService->isStoneExistsFromPost($record['post_id'])) {
                    $user = $this->userService->getOrCreateUser($record);
                    $this->stoneService->createStone($record, $user);

                    $stonesProcessed++;
                    $io->success("OK\n");
                } else {
                    $stonesIgnored++;
                    $io->warning("Already exists\n");
                }
            } catch (\Exception $e) {

                $stonesError++;
                $errorMessages[$record['post_id']] = $e->getMessage();
                $io->error($e->getMessage());
                $this->logger->error($e->getMessage());

                if (!$this->em->isOpen()) { //In case of some errors, need to reset em
                    try {
                        $this->managerRegistry->resetManager();
                    } catch (ORMException $e) {
                        $message = "Can't proceed with data due: " .$e->getMessage() ;
                        $io->text($message);
                        $this->logger->error($message);
                        break;
                    }
                }
            }
        }

        $stats = "Processed: $stonesProcessed, Ignored: $stonesIgnored, Error: $stonesError.";
        $io->success($stats);
        $this->logger->info($stats);

        foreach ($errorMessages as $key => $eMessage) {
            $io->text($key . " " . $eMessage);
        }

        return Command::SUCCESS;
    }
}
