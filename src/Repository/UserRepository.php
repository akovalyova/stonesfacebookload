<?php

declare(strict_types=1);

namespace App\Repository;

use ApiPlatform\State\Pagination\ArrayPaginator;
use App\Entity\Stone;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

/**
 * @extends ServiceEntityRepository<User>
 *
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public const ITEMS_PER_PAGE = 100;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function save(User $entity, bool $flush = false) : void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(User $entity, bool $flush = false) : void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword) : void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(\sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newHashedPassword);

        $this->save($user, true);
    }

//    /**
//     * @return User[] Returns an array of User objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('u')
//            ->andWhere('u.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('u.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

    public function findOneByEmail(string $value) : ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.email = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }


    public function findAllSortedByStonesCount(int $page = 1, array $filters = []): Query
    {
        $firstResult = ($page - 1) * self::ITEMS_PER_PAGE;


        $qb = $this->createQueryBuilder('u');
        $qb->select('u', 'COUNT(s.id) as stonesCount', 'COUNT(s2.id) as stonesOriginalCount',)
            ->leftJoin('u.stones', 's')
            ->leftJoin(Stone::class, 's2', Join::WITH, 's.id = s2.id and s2.original=true');

        if (isset($filters['lname'])) {

            $qb->andWhere($qb->expr()->orx(
                $qb->expr()->like( 'u.lname', ':word '),
                $qb->expr()->like( 'u.fname', ':word '),
                $qb->expr()->like( 'u.mname', ':word '),
                $qb->expr()->like( 'u.email', ':word '),
            ));

            $qb ->setParameter('word', '%'.$filters['lname'].'%');
        }

        return $qb->groupBy('u.id')
            ->orderBy('stonesCount', 'DESC')
            ->getQuery()
            ->setFirstResult($firstResult)
            ->setMaxResults(self::ITEMS_PER_PAGE);
    }
}
