# StonesFacebookLoad



## Getting started

	src/Command/LoadStonesFromFacebookCommand.php
Command that loads data from Facebook group - Kaminky. It can load whether from parse CSV files from *Facebook scrapper* or from *Facebook API*. 

    src/Service/Strategy
 example of Abstrac class extension and Interface implementation

With Strategy we implement Dependency Injection of either **FacebookFileService** or **FacebookApiService**.
Both services extend Abstract class that has common functionality to save images (either to disk or S3 depending on the storage) and a load external file with curl. 

	config/services.yaml
Contains parameters for Services that can be switched if needed. 


Also  LoadStonesFromFacebookCommand.php uses **StoneService**, **StoneLocationService**, **StonePhotoService** and **UserService** to save extracted data to Models. 

	src/Service/StoneLocationService.php
Gets Geocoding data from *Google Geocoding* service the string, which pressumably contains address or location written in a free form by user. 